const mysql = require( 'mysql' )


module.exports = () => {
    let config = {
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'snort_log'
    }
    
    return new Database(config)
}

class Database {
    constructor( config ) {
        this.connection = mysql.createConnection( config )
    }

    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err )
                resolve( rows )
            } )
        } )
    }

    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err )
                resolve()
            } )
        } )
    }

    beginTransaction() {
        return new Promise( ( resolve, reject ) => {
            this.connection.beginTransaction( err => {
                if ( err )
                    return reject( err )
                resolve()
            } )
        } )
    }

    commitTransaction() {
        return new Promise( ( resolve, reject ) => {
            this.connection.commit( err => {
                if ( err )
                    return reject( err )
                resolve()
            } )
        } )
    }
    
    rollback(){
        return new Promise( ( resolve, reject ) => {
            this.connection.rollback( err => {
                if ( err )
                    return reject( err )
                resolve()
            } )
        } )
    }
}
const crypto = require('crypto')
const sanitize = require('sanitize-html')

const secret = 'snort_log';

module.exports.sanitize_body = (dirty_json) => {
    let clean_json = {}

    for (let k in dirty_json) {
        clean_json[k] = sanitize(dirty_json[k], {
            allowedTags: [],
            allowedAttributes: {},
            allowedIframeHostnames: []
        }).trim();
    }

    return clean_json
}

module.exports.hash_string_sha256 = (string) => {
    return crypto.createHmac('sha256', secret).update(string).digest('hex');
}
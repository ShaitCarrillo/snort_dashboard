const conn = require('../database/dbconn')()
const data_treatment = require('../common/data_treatment')

module.exports.getOverview = (req, res) => {
    conn.query('CALL GetAttacks()').then(rows => {
        res.status(200).json(rows[0][0])
    }, err => {
        console.error(err)
    })
}

module.exports.getTopTen = (req, res) => {
    conn.query('CALL GetTopTen()').then(rows => {
        res.status(200).json(rows[0])
    }, err => {
        console.error(err)
    })
}

module.exports.getTopTenAttacks = (req, res) => {
    conn.query('CALL GetTopTenAttacks()').then(rows => {
        res.status(200).json(rows[0])
    }, err => {
        console.error(err)
    })
}

module.exports.getAllRaw = (req, res) => {
    conn.query('CALL GetAllRaw()').then(rows => {
        res.status(200).json(rows[0])
    }, err => {
        console.error(err)
    })
}

module.exports.getAllIP = (req, res) => {
    conn.query('CALL GetAllIP()').then(rows => {
        res.status(200).json(rows[0])
    }, err => {
        console.error(err)
    })
}

module.exports.setBanned = async (req, res) => {
    const fs = require('fs');

    if(fs.readFileSync("logs/tobanips.txt","utf8") == ''){
        await conn.query('CALL cleanDirty()').then(rows => {} , err => { console.error(err) })
    }

    conn.query('CALL setBanned(?)', data_treatment.sanitize_body(req.body).IP).then(rows => {
        res.status(200).json({ msg: rows[0][0].MSG })
        return conn.query('CALL getDirty()')
    }, err => {
        console.error(err)
    }).then(rows => {
        let ips = ''
        for(let row of rows[0])
            ips += `${row.IP}\n`
        fs.writeFile("logs/tobanips.txt", ips, function (err) {
            if (err) {
                return console.log(err);
            }
        });
    }, err => {
        console.error(err)
    })
}


const jwt = require('jsonwebtoken')
const fs = require('fs');
const data_treatment = require('../common/data_treatment')

module.exports.login = (req, res) => {
    const clean_body = data_treatment.sanitize_body(req.body)
    let credentials = JSON.parse(fs.readFileSync('back/services/login_credentials.json'))
    let password = data_treatment.hash_string_sha256(clean_body.password)
    let authed = false

    for(let authorizedCredentials of credentials){
        if(clean_body.user == authorizedCredentials.username  && password == authorizedCredentials.password){
            const token = jwt.sign({
                status : 'siged',
                username : 'valid'
            },process.env.PRIVATE_KEY,{
                expiresIn : '3h',
                algorithm : 'RS256'
            })
            res.status(200).json({token})
            authed = true
            break
        }
    }

    if(!authed){
        res.status(401).json({msg : "Credenciales inválidas"})
    }
}

const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const engine = require('ejs-mate')
const https = require('https')
const helmet = require('helmet')
const fs = require('fs')

const app = express()
const secureApp = express.Router()

process.env.NODE_ENV = 'production'
process.env.PRIVATE_KEY = fs.readFileSync( 'certificate/snortdashboard.key')
process.env.PUBLIC_KEY = fs.readFileSync('certificate/snortdashboard.crt')

app.use(helmet())
app.set('port', process.env.PORT || 8080)
app.set('views', path.join(__dirname, 'front'))
app.use(express.static(__dirname + '/front'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.engine('ejs', engine)
app.set('view engine', 'ejs')


secureApp.use(express.urlencoded({ extended: false }))
app.use(express.urlencoded({ extended: false }))


app.get('/*',function(req,res,next){
    res.header('X-XSS-Protection' , 1 )
    res.header('Cache-Control', 'no-cache')
    res.header('X-Content-Type-Option' ,  'nosniff')
    next();
})

app.use('/secured', secureApp)
require('./router/web_server')(app)
require('./router/data_server')(secureApp, app)
require('./file_interpreter').startObserver()

app.use((err, req, res, next) => {
    if (err) {
        console.error(err)
        return res.sendStatus(500)
    }
    next()
})

https.createServer({
    key: fs.readFileSync(__dirname + '/certificate/snortdashboard.key'),
    cert: fs.readFileSync(__dirname + '/certificate/snortdashboard.crt')
}, app).listen(app.get('port'), () => {
    console.log('Listen', app.get('port'))
})
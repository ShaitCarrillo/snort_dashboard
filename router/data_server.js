const data_collector = require('../back/services/data_collector')
const login_services = require('../back/services/login_services')
const jwt = require('jsonwebtoken')


module.exports = (secureApp, app)=>{
    
    app.post('/login' , login_services.login)

    secureApp.use(function (req,res,next) {
        var token = req.body.token
        res.header('X-XSS-Protection' , 1 )
        res.header('Cache-Control', 'no-cache')
        res.header('X-Content-Type-Option' ,  'nosniff')
        if(token){
            jwt.verify(token,process.env.PUBLIC_KEY,function (err,decode) {
                if(err){
                    res.status(401).json({
                        msg : 'Token invalido'
                    })
                }else{
                    next()
                }
            })
        }else{
            res.status(401).json({
                mg : 'Sin token'
            })
        }
    })

    secureApp.post('/getOverview' , data_collector.getOverview)
    secureApp.post('/getTopTen' , data_collector.getTopTen)
    secureApp.post('/getTopTenAttacks' , data_collector.getTopTenAttacks)
    secureApp.post('/getAllRaw' , data_collector.getAllRaw)
    secureApp.post('/getAllIP' , data_collector.getAllIP)
    secureApp.post('/setBanned' , data_collector.setBanned)
}
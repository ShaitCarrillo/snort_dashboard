module.exports = (app)=> {

    app.get('/overview' , (req,res) => {
        res.render('overview' , {info : 'Overview'})
    })

    app.get('/tableview' , (req,res) => {
        res.render('tableview' , {info : 'Tables'})
    })

    app.get('/ban' , (req,res) => {
        res.render('block_ip' , {info : 'Block'})
    })

    app.get('/' , (req,res) => {
        res.render('login' , {info : ''})
    })

    
}

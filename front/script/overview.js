axios({
    method: 'post',
    url: `https://${document.domain}:8080/secured/getOverview`,
    responseType: 'json',
    data: {
        token : sessionStorage.getItem('token')
    },
    rejectUnauthorized: false
}).then(function (response) {
    let data = response.data
    drawOverview([data.ZERO, data.ONE, data.TWO, data.THREE, data.FOUR])
}).catch(handleError)

axios({
    method: 'post',
    url: `https://${document.domain}:8080/secured/gettopten`,
    responseType: 'json',
    data: {
        token : sessionStorage.getItem('token')
    },
    rejectUnauthorized: false
}).then(function (response) {
    drawTopTen(response.data)
}).catch(handleError)

axios({
    method: 'post',
    url: `https://${document.domain}:8080/secured/gettoptenattacks`,
    responseType: 'json',
    data: {
        token : sessionStorage.getItem('token')
    },
    rejectUnauthorized: false
}).then(function (response) {
    drawTopTenAttacks(response.data)
}).catch(handleError)

function drawTopTen(data) {
    let ctx = document.getElementById('topten').getContext('2d')
    let labels = []
    let attacks = {
        ZERO: [],
        ONE: [],
        TWO: [],
        THREE: [],
        FOUR: []
    }
    let datasets = []
    for (let element of data) {
        labels.push(element.IP_ORIGIN)
        attacks.ZERO.push(element.ZERO)
        attacks.ONE.push(element.ONE)
        attacks.TWO.push(element.TWO)
        attacks.THREE.push(element.THREE)
        attacks.FOUR.push(element.FOUR)
    }
    const backColors = [
        '#3C153B',
        '#3B69A7',
        '#00492C',
        '#DB4C40',
        '#A32136'
    ]
    const priorities = ['ZERO', 'ONE', 'TWO', 'THREE', 'FOUR']

    for (let index = 0; index < 5; index++) {
        let dataset = {
            label: priorities[index],
            data: attacks[priorities[index]],
            backgroundColor: backColors[index],
            borderColor: '#FFFFFF',
            borderWidth: 1
        }
        datasets.push(dataset)
    }

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: datasets
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: 1,
                        zeroLineWidth: 1,
                        zeroLineColor: '#fff'
                    },
                }],
                xAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: .5,
                    }
                }]
            },
        }
    })
}

function drawTopTenAttacks(data) {
    let ctx = document.getElementById('toptenattacks').getContext('2d')
    let labels = []
    let values = []

    for (let element of data) {
        labels.push(element.TITLE)
        values.push(element.ATTACKS)
    }

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Attacks',
                data: values,
                backgroundColor: '#3C153B',
                borderColor: '#FFFFFF',
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: .5
                    },
                }],
                xAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: 1,
                        zeroLineWidth: 1,
                        zeroLineColor: '#fff'
                    }
                }]
            },
        }
    })
}

function drawOverview(data) {
    var ctx = document.getElementById('myChart').getContext('2d')
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Prioridad 0', 'Prioridad 1', 'Prioridad 2', 'Prioridad 3', 'Prioridad 4'],
            datasets: [{
                label: 'Attacks',
                data: data,
                backgroundColor: [
                    '#3C153B',
                    '#3B69A7',
                    '#00492C',
                    '#DB4C40',
                    '#A32136'
                ],
                borderColor: [
                    '#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF'
                ],
                borderWidth: 1
            }],
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: 1,
                        zeroLineWidth: 1,
                        zeroLineColor: '#fff'
                    },
                }],
                xAxes: [{
                    gridLines: {
                        color: "#a19e9d",
                        lineWidth: .5,
                    }
                }]
            },
        }
    })
}


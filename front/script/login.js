function login() {  
    let user = document.getElementById('username').value
    let password = document.getElementById('password').value
    let message = ''

    if(user == '') message+='Nombre de usuario vacío'
    if(password == '') {
        if(message != '') message+='<br>'
        message+='Contraseña vacía'
    }
    if(message != '') {
        M.toast({html: message})
        return
    }

    axios({
        method: 'post',
        url: `https://${document.domain}:8080/login`,
        responseType: 'json',
        data: {
            user,
            password
        },
        rejectUnauthorized: false
    }).then(function (response) {
        sessionStorage.setItem('token' , response.data.token)
        window.open(`https://${document.domain}:8080/overview` , '_self')
    }).catch(error => { 
        if (error.response) {
            M.toast({html : error.response.data.msg})
        }
     })
}

if(sessionStorage.getItem('error')){
    M.toast({html : sessionStorage.getItem('error')})
    sessionStorage.removeItem('error')
}

document.getElementById('login').addEventListener('click' , login)

document.addEventListener('keypress', evt => {
    console.log('eventos')
    if(evt.keyCode == 13) login()
}, false)
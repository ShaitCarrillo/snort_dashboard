const tableBody = document.getElementById('tableBody')
var allData = []

document.getElementById('setFilters').addEventListener('click', set_filter)
document.getElementById('updateData').addEventListener('click', getData)

let start_date = document.getElementById('start')
let end_date = document.getElementById('end')

start_date.addEventListener('change', () => {
    let date = new Date(M.Datepicker.getInstance(start_date).toString())
    date.setDate(date.getDate() + 2)
    let last_date = M.Datepicker.getInstance(end_date).toString()

    M.Datepicker.init(end_date, { minDate: date, format: 'yyyy-mm-dd' })

    let end = M.Datepicker.getInstance(end_date)    
    
    if( parseInt(end_date.value.replace(/\-/g, '')) < parseInt(start_date.value.replace(/\-/g, '')) ){
        end.setDate(date)
        end_date.value = end.toString()
    }else{
        let date_to_set = new Date(last_date)
        date_to_set.setDate(date_to_set.getDate() + 1)
        end.setDate(date_to_set)
    }
})

let date_filter = 'all'

let radios = document.forms["timeSearch"].elements["dateParam"]
for (var i = 0, max = radios.length; i < max; i++) {
    radios[i].onclick = function () {
        date_filter = this.value
        switch (this.value) {
            case 'all':
                start_date.disabled = true
                end_date.disabled = true
                break;
            case 'oneDay':
                start_date.disabled = false
                end_date.disabled = true
                break;
            case 'range':
                start_date.disabled = false
                end_date.disabled = false
                break;
        }
    }
}


getData()

function getData() {
    axios({
        method: 'post',
        url: `https://${document.domain}:8080/secured/getAllRaw`,
        responseType: 'json',
        data: {
            token : sessionStorage.getItem('token')
        },
        rejectUnauthorized: false
    }).then(function (response) {
        allData = response.data
        set_filter()
    }).catch(err => { handleError })
}

function set_filter() {
    let priority_filter = []

    tableBody.innerHTML = ''

    if (document.getElementById('zero').checked) priority_filter.push(0)
    if (document.getElementById('one').checked) priority_filter.push(1)
    if (document.getElementById('two').checked) priority_filter.push(2)
    if (document.getElementById('three').checked) priority_filter.push(3)
    if (document.getElementById('four').checked) priority_filter.push(4)

    let start = parseInt(M.Datepicker.getInstance(start_date).toString().replace(/\-/g, ''))
    let end = parseInt(M.Datepicker.getInstance(end_date).toString().replace(/\-/g, ''))

    switch (date_filter) {
        case 'all':
            start = 00000000
            end = 99999999
            break;
        case 'oneDay':
            end = start
            break;
        default:
            break;
    }

    for (let element of allData) {
        if (priority_filter.indexOf(element.PRIORITY) > -1 && parseInt(element.DATE_RAW) >= start && parseInt(element.DATE_RAW) <= end) {
            let row = `<tr>
                <td>${element.DATE} : ${element.TIME}</td>
                <td>${element.TITLE}</td>
                <td>${element.CLASS}</td>
                <td>${element.PRIORITY}</td>
                <td>${element.ORIGIN}</td>
                <td>${element.DESTINATION}</td>
                <td>${element.PORT_DESTINATION}</td>
                <td>${element.PORT_DESC}</td>
                <td>${element.ATTACKS}</td> 
            </tr>`
            tableBody.innerHTML += row
        }
    }
}
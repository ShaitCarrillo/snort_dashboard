M.Modal.init(document.querySelectorAll('.modal') , {dismissible : false});

getData()

function getData(){
    axios({
        method: 'post',
        url: `https://${document.domain}:8080/secured/getAllIP`,
        responseType: 'json',
        data: {
            token: sessionStorage.getItem('token')
        },
        rejectUnauthorized: false
    }).then(function (response) {
        drawTopTen(response.data)
        addBlockEvent()
    }).catch(err => { handleError })
}

function ban(IP){
    axios({
        method: 'post',
        url: `https://${document.domain}:8080/secured/setBanned`,
        responseType: 'json',
        data: {
            token: sessionStorage.getItem('token'),
            IP
        },
        rejectUnauthorized: false
    }).then(function (response) {
        M.toast({html : response.data.msg})
        getData()
    }).catch(err => { handleError })
}


const table = document.getElementById('tableBody')
let actual_to_ban = ''

document.getElementById('banManual').addEventListener('click' , () => {
    let actual_to_ban_verify = document.getElementById('ip_address_manual').value
    if(actual_to_ban_verify == ''){
        M.toast({html : 'Ingresa una IP'})
        return
    }
    actual_to_ban = actual_to_ban_verify
    document.getElementById('toblock').innerHTML = actual_to_ban
    let instance = M.Modal.getInstance(document.getElementById('ban_ip'))
    instance.open()
})
document.getElementById('summit').addEventListener('click' , () => {
    ban(actual_to_ban)
})

document.getElementById('cancel').addEventListener('click' , () => {
    actual_to_ban = ''
})

function addBlockEvent(){

    let elements = document.getElementsByClassName('a-trigger')

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', (evt) => {
            actual_to_ban = evt.srcElement.dataset.ip
            document.getElementById('toblock').innerHTML = actual_to_ban
            let instance = M.Modal.getInstance(document.getElementById('ban_ip'))
            instance.open()
        }, false);
    }
}

function drawTopTen(data) {
    table.innerHTML = ''
    for (let element of data) {
        let row = `<tr>
            <td>${element.IP_ORIGIN}</td>
            <td>${element.ZERO}</td>
            <td>${element.ONE}</td>
            <td>${element.TWO}</td>
            <td>${element.THREE}</td>
            <td>${element.FOUR}</td>
            <td>${element.TOTAL}</td>
            <td>`
        if(element.BANNED_STAT == 0) row+=`<a class='a-trigger' href='#' data-ip="${element.IP_ORIGIN}"><i data-ip="${element.IP_ORIGIN}" class="material-icons red-text">block</i></a></td></tr>`
        else row+=`</td></tr>`
        table.innerHTML += row
    }
}

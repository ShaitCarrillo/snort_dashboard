document.addEventListener('DOMContentLoaded', () => {
    M.Sidenav.init(document.querySelectorAll('.sidenav'))
    M.Collapsible.init(document.querySelectorAll('.collapsible'))
    M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'))
    M.Tabs.init(document.querySelectorAll('.tabs'));
    M.Datepicker.init(document.querySelectorAll('.datepicker'), { format: 'yyyy-mm-dd' });
})

function handleError(err){
    if(err.response.status === 401){
        sessionStorage.setItem('error' , 'Debes iniciar sesión')
        setTimeout(()=>{
            window.open('/' , '_self')
        })
    }
}


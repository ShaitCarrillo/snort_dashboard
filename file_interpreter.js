const fs = require('fs')
const conn = require('./back/database/dbconn')()

module.exports.startObserver = () => {
    
    const file = fs.readFileSync(__dirname + '/logs/alert.txt', 'utf-8')
    // console.log(interpreter(file))
    // return
    dbInsert(interpreter(file))
    
    fs.watchFile(__dirname + '/logs/alert.txt' , (curr,prev) =>{
        let date = new Date()
        // console.log(`Cambio detectado ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`);
        
        const file = fs.readFileSync(__dirname + '/logs/alert.txt', 'utf-8')
        dbInsert(interpreter(file))
    })
}

async function dbInsert(data){
    if(!data) return
    for(const element of data){
        var path_ids = []
        for ( const element_domain of element.domains){
            let id
            await conn.query('call InsertURL ( ? , ? )' , [element_domain.domain , element_domain.path]).then(rows => {
                // console.log(rows[0][0].ID_PATH)
                id = rows[0][0].ID_PATH
            } , err => {
                throw err
            }).catch(err => {
                // console.log(err)
            })
            path_ids.push(id)
        }

        let data = [
            element.title,
            `${element.date} ${element.time}`,
            element.clasification,        
            element.priority,
            element.ip_origin,
            element.ip_destination,
            element.port_origin,
            element.port_destination,
            element.sequence
        ]

        let entry_id
        // console.log(data)
        await conn.query('call InsertEntry( ? , ? , ? , ? , ? , ? , ? , ? , ? ) ' , data).then(rows => {
            entry_id = rows[0][0].ID_ENTRY
        }, err => {
            throw err
        }).catch(error => {
            console.log(error)
        })

        for(const id of path_ids){
            await conn.query('call InsertRefers( ? , ? )' , [id , entry_id]).then(rows => {
                // console.log(rows)
            }, err => {
                throw err
            }).catch(error => {
                console.log(error)
            })
        }

    }
}

function interpreter(file) {
    // En algunos entornos es necesario sustituir por \r\n\r\n
    const array = file.split('\n\n')
    const data = []
    const date = new Date()
    if(file == '') return false
    array.forEach(element => {
        // En algunos entornos es necesario sustituir por \r\n\
        let lines = element.split('\n')
        let title = lines[0].match(/[0-9]\]\s(.*?)\s\[/g)[0]

        
        let desc_and_priority = lines[1].match(/\[(.*?)\]\s/g)
        if(desc_and_priority.length == 1) desc_and_priority.unshift("NI")
        let date_time = lines[2].split(' ')
        date_time.splice(2,1)
        let date_raw = date_time[0].split('-')
        let date_complete = `${date.getFullYear()}-${date_raw[0].split('/')[0]}-${date_raw[0].split('/')[1]}`
        let sequence = lines[4].split(" ")[2]
        
        let entry = {
            title : title.substring(3,title.length-2),
            priority : desc_and_priority[1].replace('[' , '').replace(']' , '').replace('Priority: ' , ''),
            clasification : desc_and_priority[0].replace('[' , '').replace(']' , '').replace('Classification: ' , ''),
            date : date_complete,
            time :  date_raw[1],
            ip_origin : date_time[1].split(':')[0],
            port_origin : date_time[1].split(':')[1],
            ip_destination : date_time[2].split(':')[0],
            port_destination : date_time[2].split(':')[1],
            sequence : sequence
        }

        let xref = []
        if(lines.length == 7){
            lines[6].match(/\[(.*?)\]/g).forEach(element =>{
                let domain = element.match(/http:\/\/(.*?)\//g)[0].replace('[' , '').replace(']' , '')
                let path = element.replace(domain , '').replace('[' , '').replace(']' , '').replace('Xref => ' , '')
                xref.push({domain : domain , path : path})
            })
            entry.domains = xref
        }else{
            entry.domains = []
        }
        data.push(entry)
    })
    return data    
}
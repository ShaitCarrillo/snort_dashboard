-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-11-2019 a las 19:08:29
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `snort_log`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ATTEMPT`
--

CREATE TABLE `ATTEMPT` (
  `ID` int(11) NOT NULL,
  `ENTRY` int(11) NOT NULL,
  `SEQUENCE` varchar(15) NOT NULL,
  `DATE_TIME` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `BANNED`
--

CREATE TABLE `BANNED` (
  `ID` int(11) NOT NULL,
  `IP` varchar(45) NOT NULL,
  `TIMESTAMP` datetime NOT NULL,
  `DIRTY` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CATALOGUE`
--

CREATE TABLE `CATALOGUE` (
  `PORT` varchar(10) NOT NULL,
  `DESC` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CLASS`
--

CREATE TABLE `CLASS` (
  `ID` int(11) NOT NULL,
  `CLASS` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DOMAIN`
--

CREATE TABLE `DOMAIN` (
  `ID` int(11) NOT NULL,
  `DOMAIN` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ENTRY`
--

CREATE TABLE `ENTRY` (
  `ID` int(11) NOT NULL,
  `TITLE` int(11) NOT NULL,
  `CLASS` int(11) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `ORIGIN` varchar(45) NOT NULL,
  `DESTINATION` varchar(45) NOT NULL,
  `PORT_ORIGIN` varchar(10) NOT NULL,
  `PORT_DESTINATION` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ENTRY_id`
--

CREATE SEQUENCE `ENTRY_ID` START WITH 1 INCREMENT BY 1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PATH`
--

CREATE TABLE `PATH` (
  `ID` int(11) NOT NULL,
  `DOMAIN` int(11) NOT NULL,
  `PATH` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `REFERS`
--

CREATE TABLE `REFERS` (
  `ID` int(11) NOT NULL,
  `PATH` int(11) NOT NULL,
  `ENTRY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `title`
--

CREATE TABLE `title` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ATTEMPT`
--
ALTER TABLE `ATTEMPT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_ENTRY_related` (`ENTRY`);

--
-- Indices de la tabla `BANNED`
--
ALTER TABLE `BANNED`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `CATALOGUE`
--
ALTER TABLE `CATALOGUE`
  ADD PRIMARY KEY (`PORT`);

--
-- Indices de la tabla `CLASS`
--
ALTER TABLE `CLASS`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `DOMAIN`
--
ALTER TABLE `DOMAIN`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `ENTRY`
--
ALTER TABLE `ENTRY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_title_id` (`TITLE`),
  ADD KEY `fk_CLASS_id` (`CLASS`);

--
-- Indices de la tabla `PATH`
--
ALTER TABLE `PATH`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `key_doain_idx` (`DOMAIN`);

--
-- Indices de la tabla `REFERS`
--
ALTER TABLE `REFERS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `key_reference_idx` (`PATH`),
  ADD KEY `key_ENTRY_idx` (`ENTRY`);

--
-- Indices de la tabla `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ATTEMPT`
--
ALTER TABLE `ATTEMPT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `BANNED`
--
ALTER TABLE `BANNED`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `CLASS`
--
ALTER TABLE `CLASS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `DOMAIN`
--
ALTER TABLE `DOMAIN`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ENTRY`
--
ALTER TABLE `ENTRY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `PATH`
--
ALTER TABLE `PATH`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `REFERS`
--
ALTER TABLE `REFERS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `title`
--
ALTER TABLE `title`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ATTEMPT`
--
ALTER TABLE `ATTEMPT`
  ADD CONSTRAINT `fk_ENTRY_related` FOREIGN KEY (`ENTRY`) REFERENCES `ENTRY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ENTRY`
--
ALTER TABLE `ENTRY`
  ADD CONSTRAINT `fk_CLASS_id` FOREIGN KEY (`CLASS`) REFERENCES `CLASS` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_title_id` FOREIGN KEY (`TITLE`) REFERENCES `title` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `PATH`
--
ALTER TABLE `PATH`
  ADD CONSTRAINT `key_doain` FOREIGN KEY (`DOMAIN`) REFERENCES `DOMAIN` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `REFERS`
--
ALTER TABLE `REFERS`
  ADD CONSTRAINT `key_ENTRY` FOREIGN KEY (`ENTRY`) REFERENCES `ENTRY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `key_reference` FOREIGN KEY (`PATH`) REFERENCES `PATH` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
